<?php

namespace Iabsis\VideothequeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Picture
 *
 * @ORM\Table(name="picture")
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks
 */
class Picture
{
    /******************************************************************************************************************
     *                                          Attributes
     *****************************************************************************************************************/

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank
     */
    protected $name;


    private $file;

    /**
     * @ORM\Column(name="path", type="string", length=255, nullable=true)
     */
    protected $path;


    /**
     * @ORM\OneToOne(targetEntity="Film", mappedBy="picture")
     */
    protected $film;

    private $tempFilename;

    /*******************************************************************************************************************
     *                                      Getter and Setter
     ******************************************************************************************************************/

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }
    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
        $this->path = $this->modifyName();


    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Document
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set path
     * @param string $path
     * @return Document
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set film
     *
     * @param \Iabsis\VideothequeBundle\Entity\Film $film
     * @return Picture
     */
    public function setFilm(\Iabsis\VideothequeBundle\Entity\Film $film = null)
    {
        $this->film = $film;
        return $this;
    }

    /**
     * Get film
     *
     * @return \Iabsis\VideothequeBundle\Entity\Film
     */
    public function getFilm()
    {
        return $this->film;
    }


    /******************************************************************************************************************
     *                                  Methodes
     *****************************************************************************************************************/

    private function modifyName(){
        $date = new \DateTime();
        $name = $date->getTimestamp().$this->file->getClientOriginalName();
        return $name;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/imgs';
    }



    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        // la propriété « file » peut être vide si le champ n'est pas requis
        if (null === $this->file) {
            return;
        }

        if (!file_exists($this->getUploadRootDir())) {
            mkdir($this->getUploadRootDir(), 0775, true);
        }
        $this->file->move(
            $this->getUploadRootDir(), $this->path
        );
        $this->file = null;
    }


    /**
     * @ORM\PreUpdate()
     * @ORM\PreRemove()
     */
    public function preRemoveUpload()
    {
        // On sauvegarde temporairement le nom du fichier, car il dépend de l'id
        $this->tempFilename = $this->getUploadRootDir().'/'.$this->path;
    }

    /**
     * @ORM\PostUpdate()
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        // En PostRemove, on n'a pas accès à l'id, on utilise notre nom sauvegardé
        if (file_exists($this->tempFilename)) {
            // On supprime le fichier
            unlink($this->tempFilename);
        }
    }


    public function __toString()
    {
        return $this->name;
    }
}
